The pattern generator has the following registers:
0) control
1) status
2) remaining_capacity
3) runs_number
4) pattern_duration
5) pattern_output
6) pattern_condition

0) control (write only, automatically returns to 0 after writing):
    [0] start generator
    [1] stop generator after finishing the current run
    [2] stop immediatelly (reset)
    [3] push pattern into memory
 [31:4] reserved
   
1) status (read only)
    [0] generator running
 [31:1] reserved

2) remaining_capacity (read only)
    unsigned integer [$clog2(PATTERN_MEM_DEPTH)] - number of remaining positions in pattern memory. Decreases with each push to the memory.

3) runs_number (read, write)
    unsigned integer [RUN_COUNT_WIDTH] - number of runs. If set to zero, the pattern generator keeps running until it's stopped manually. Otherwise is stops after the specified number of runs.

4) pattern_duration (read, write)
    unsigned integer [TIMER_WIDTH] - A duration of the pattern in clk cycles (10ns / 100 MHz  in case of CLICTD)
  
5) pattern_output (read, write)
    bit array [SIGNALS_N] - Defines the satate of the output signals during the pattern duration.
    
6) pattern_condition (read, write)
    bit array [3 + 3*TRIGGERS_N] A condition of input signals to continue run the pattern timer. This is evaluated at the beginning of the pattern time

    The condition consists of a global condition that is applied to all inputs (3 most-significant bits):
            |   OR 001
           ~|  NOR 101
            &  AND 011
           ~& NAND 111
            ^  XOR 010
           ~^ XNOR 110
     "always true" 000
          reserved 100
    
    And 3 bits for each input signal, specifying the desired condition of the signal:
      H High       100
      L Low        101
      R Rising     001
      F Falling    010
      E Edge (any) 011
      A Always     111
      N Never      000
        reserved   110
      
Registers 4, 5 and 6 are pushed to the next position in pattern memory when bit 3 of register 0 is set to 1. The pattern memory pointer is reset to begining when a pattern with zero duration is pushed.

Each push to the pattern memory resets (stops) the generator. To prevent unitended behaviour, it is recommended to configure the generator only when it is not running.

How to configure the pattern generator:
---------------------------------------
1) Set reg. 4 (pattern_duration) to 0.
2) Set reg. 0 (control) to 0x8 (push pattern into memory)
* The memory pointer is now set to the beginning of the memory.
3) Set registers 4, 5 and 6 with the desired output pattern, time duration and trigger condition.
4) Set reg. 0 (control) to 0x8 (push pattern into memory)
5) Repeat steps 3) and 4) until you program a desired waveform or reach the end of pattern memory (reg. 2 (remaining_capacity) == 0)
6) Configure number of runs through the pattern memory (reg. 3 (runs_number)). If set to 0, the generatoru will repeat the waveform until it's stopped manually.
7) You can run the generator.

How to run the configured generator:
------------------------------------
1) Start the generator by writing 0x1 to reg. 0 (control - start generator)
* The generator will load the first pattern from the memory, set the outputs and wait for a trigger condition to be satisfied. Then it will start counting the time.
* When the time runs out, it will continue with the next pattern. 
* If it was the last pattern in the memory but not a last run, it will start a next run with the first pattern again.
* If the run number was equal to the programmed number of runs, is will stop at the last pattern in the memory.

2) Stop the generator by  writing 0x2 to reg. 0 (control - stop generator after finishing the current run) or wait until it finishes the programmed number of runs. 
   You can also stop the generator immediatelly by writing 0x4 to reg. 0 (control - stop immediatelly (reset)). The outputs will stay in the last state.

