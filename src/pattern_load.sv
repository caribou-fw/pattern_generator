`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: CERN
// Engineer: Tomas Vanat
// 
// Design Name: Pattern Generator 
// Module Name: pattern_load
// Project Name: Caribou
// Target Devices: Caribou
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pattern_load #(
    int unsigned PATTERN_MEM_DEPTH = 32,
    int unsigned RUN_COUNT_WIDTH = 8,
    int unsigned TIMER_WIDTH = 32,
    int unsigned SIGNALS_N = 7,
    int unsigned TRIGGERS_N = 2
)(
    input clk,
    input rst,

    output mem_read_strobe,
    output [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_read_address,
    input [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_last_address,
    
    input [TIMER_WIDTH-1 : 0] timer_pattern_conf,
    input [(TRIGGERS_N*3)+2 : 0] triggers_pattern_conf,
    input [(TRIGGERS_N)-1 : 0] triggers_in,
    input [RUN_COUNT_WIDTH-1 : 0] conf_runs_n,
    input conf_run_terminate,
    input conf_run_start,

    output running
);

  logic pattern_memory_bottom_ptr = (PATTERN_MEM_DEPTH);


  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] pattern_mem_ptr; 
  logic [RUN_COUNT_WIDTH-1 : 0] runs_count;
  logic run_last;
  logic [TIMER_WIDTH-1 : 0] timer;
  logic timer_isone;
  logic mem_load_next;
  
  logic [(TRIGGERS_N)-1 : 0] triggers_prev;
  logic [(TRIGGERS_N)-1 : 0] triggers;
  logic trigger_global;
  
  logic gen_start;
  logic running_int;
  logic terminate_request;
  logic run_end;
  logic next_run;
  logic gen_rst;
  
  assign mem_read_address = pattern_mem_ptr;
  assign running = running_int;
  assign mem_read_strobe = mem_load_next && !(run_last && run_end);

  // ///////////////////////
  // /// trigger signals ///
  // ///////////////////////
  //
  // configuration of individual signals:
  // H High       100
  // L Low        101
  // R Rising     001
  // F Falling    010
  // E Edge (any) 011
  // A Always     111
  // N Never      000
  // X reserved   110
  //
  // configuration of global trigger condition:
  //   OR 001
  //  NOR 101
  //  AND 011
  // NAND 111
  //  XOR 010
  // XNOR 110
  //  '1' 000
  // --   100
  //
  
  // capture previous state
  always_ff @(posedge clk)
    triggers_prev <= triggers_in;
  
  generate
    // individual triggers based on their settings
    for (genvar i=0; i < TRIGGERS_N; i=i+1) begin
      always_comb begin
        case (triggers_pattern_conf[(3*i)+2 : (3*i)])
          // High
          3'b100 : begin
            if (triggers_in[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Low
          3'b101 : begin
            if (~triggers_in[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Rising
          3'b001 : begin
            if (triggers_in[i] & ~triggers_prev[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Falling
          3'b010 : begin
            if (~triggers_in[i] & triggers_prev[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Edge (any)
          3'b011 : begin
            if (triggers_in[i] ^ triggers_prev[i])
              triggers[i] = 1'b1;
            else
              triggers[i] = 1'b0;
          end
  
          // Always
          3'b111 : begin
            triggers[i] = 1'b1;
          end
  
          // Never
          3'b000 : begin
            triggers[i] = 1'b0;
          end
  
          // Reserved
          3'b110 : begin
            triggers[i] = 1'b0;
          end
  
          default : begin
            triggers[i] = 1'b0;
          end
          
        endcase
      end
    end
  endgenerate
  
  // global trigger condition
  always_comb begin
    case (triggers_pattern_conf[(TRIGGERS_N*3)+2 : (TRIGGERS_N*3)])
        // OR
        3'b001 : begin
          trigger_global = |triggers;
        end

        // NOR
        3'b101 : begin
          trigger_global = ~|triggers;
        end

        // AND
        3'b011 : begin
          trigger_global = &triggers;
        end

        // NAND
        3'b111 : begin
          trigger_global = ~&triggers;
        end

        // XOR
        3'b010 : begin
          trigger_global = ^triggers;
        end

        // XNOR
        3'b110 : begin
          trigger_global = ~^triggers;
        end

        // always armed
        3'b000 : begin
          trigger_global = 1'b1;
        end

        // Reserved
        3'b100 : begin
          trigger_global = 1'b0;
        end

        default : begin
          trigger_global = 1'b0;
        end

      endcase
  end

  // /////////////////////
  // /// pattern timer ///
  // ///  i.e. LOOP 1  ///
  // /////////////////////

  // pattern timer ran out of time
  always_comb begin
    if ( ~|timer[TIMER_WIDTH-1:0] )
      timer_isone = 1'b1;
    else 
      timer_isone = 1'b0;
  end  

  // pattern timer
  always_ff @(posedge clk) begin
    if (gen_rst)
      timer <= 'b0;
    else if (mem_load_next || !(trigger_global || pattern_trigger_armed))
      timer <= timer_pattern_conf;
    else if (timer_isone)
      timer <= timer;
    else
      timer <= timer - 1;
  end

  // load next pattern when time is out
  assign mem_load_next = (timer_isone && (trigger_global || pattern_trigger_armed)) || gen_start; 
  
  always_ff @(posedge clk) begin
    if (mem_load_next)
      pattern_trigger_armed <= 1'b0;
    else if (trigger_global)
      pattern_trigger_armed <= 1'b1;
    else
      pattern_trigger_armed <= pattern_trigger_armed;
    
  end

  // //////////////////////
  // /// memory pointer ///
  // ///  i.e. LOOP 2   ///
  // //////////////////////

  // note that the pointer starts from the end and counts down
  always_comb begin
    // pattern mem pointer is at the end
    if ( pattern_mem_ptr == mem_last_address )
      run_end = 1'b1;
    else 
      run_end = 1'b0;
  end  

  // pattern mem pointer
  always_ff @(posedge clk) begin
    if (gen_rst || run_end)
      pattern_mem_ptr <= PATTERN_MEM_DEPTH-1;
    else if (mem_load_next)
      pattern_mem_ptr <= pattern_mem_ptr - 1;
    else
      pattern_mem_ptr <= pattern_mem_ptr;
  end  

  // ///////////////////
  // /// run counter ///
  // /// i.e. LOOP 3 ///
  // ///////////////////

  // last run
  always_comb begin
    // request to stop
    if (terminate_request)
      run_last = 1'b1;
    // if conf_runs == 0, run forever
    else if (~|conf_runs_n)
      run_last = 1'b0;
    else if (runs_count == conf_runs_n)
      run_last = 1'b1;
    else 
      run_last = 1'b0;
  end

  assign next_run = !run_last && run_end && mem_load_next;
  assign gen_rst = (run_last && run_end && mem_load_next) || rst;

  // run no. counter
  always_ff @(posedge clk) begin
    if (gen_rst)
      runs_count <= 'b0;
    else if (next_run || gen_start)
      runs_count <= runs_count + 1;
    else
      runs_count <= runs_count;
  end

  // ///////////////////
  // /// run control ///
  // ///////////////////

  always_ff @(posedge clk) begin
    if (gen_rst)
      running_int <= 1'b0;
    else if (conf_run_start)
      running_int <= 1'b1;
    else 
      running_int <= running_int;
  end

  assign gen_start = conf_run_start && !running_int;

  always_ff @(posedge clk) begin
    if (running_int && conf_run_terminate)
      terminate_request <= 1'b1;
    else if (!running_int)
      terminate_request <= 1'b0;
    else
      terminate_request <= terminate_request;
  end

endmodule
