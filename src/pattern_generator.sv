`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.09.2019 17:57:17
// Design Name: 
// Module Name: pattern_generator
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module pattern_generator #(
    int unsigned PATTERN_MEM_DEPTH = 32,
    int unsigned RUN_COUNT_WIDTH = 8,
    int unsigned TIMER_WIDTH = 32,
    int unsigned SIGNALS_N = 7,
    int unsigned TRIGGERS_N = 2
  )(
    input clk_cfg,
    input clk_gen,
    input rst,

    input [(TRIGGERS_N)-1 : 0] triggers_in,
    output [SIGNALS_N-1 : 0] outputs_out, 

    input [TIMER_WIDTH-1 : 0] timer_pattern_conf_in,
    input [SIGNALS_N-1 : 0] output_pattern_conf_in,
    input [(TRIGGERS_N*3)+2 : 0] triggers_pattern_conf_in,
    input conf_write_pattern_in,

    input [RUN_COUNT_WIDTH-1 : 0] conf_runs_n,    
    input conf_run_terminate,
    input conf_run_start,
    output stat_gen_running
  );

  logic [TIMER_WIDTH-1 : 0] timer_pattern_conf;
  logic [SIGNALS_N-1 : 0] output_pattern_conf;
  logic [(TRIGGERS_N*3)+2 : 0] triggers_pattern_conf;
  logic conf_write_pattern;
  logic [(TRIGGERS_N)-1 : 0] triggers;
  
  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_write_address;
  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_read_address;
  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_last_address;
  logic [($clog2(PATTERN_MEM_DEPTH))-1 : 0] mem_last_address_Sgen;
  logic [(TIMER_WIDTH + SIGNALS_N + (TRIGGERS_N*3)+3)-1 : 0] mem_write_data;
  logic [(TIMER_WIDTH + SIGNALS_N + (TRIGGERS_N*3)+3)-1 : 0] mem_read_data;
  logic [TIMER_WIDTH-1 : 0] timer_pattern_mem;
  logic [SIGNALS_N-1 : 0] output_pattern_mem;
  logic [(TRIGGERS_N*3)+2 : 0] triggers_pattern_mem;
  logic [RUN_COUNT_WIDTH-1 : 0] conf_runs_n_Sgen;
  logic conf_run_terminate_Sgen;
  logic conf_run_start_Sgen;
  logic rst_gen_1;
  logic rst_gen_2;
  logic rst_gen;
  logic gen_running;
  logic mem_write_strobe;
  logic mem_read_strobe;
  logic timer_conf_notzero;
  
  always_ff @(posedge clk_cfg) begin
    timer_pattern_conf    <= timer_pattern_conf_in;
    output_pattern_conf   <= output_pattern_conf_in;
    triggers_pattern_conf <= triggers_pattern_conf_in;
    conf_write_pattern    <= conf_write_pattern_in;
    triggers              <= triggers_in;
    timer_conf_notzero    <= |timer_pattern_conf;
  end
  
  assign rst_gen = rst_gen_1 || rst_gen_2;
  assign outputs_out = output_pattern_mem;
  
  always_ff @(posedge clk_gen) begin
    if (mem_read_strobe) begin
      timer_pattern_mem    <= mem_read_data[TIMER_WIDTH+SIGNALS_N-1 : TIMER_WIDTH];
      output_pattern_mem   <= mem_read_data[TIMER_WIDTH-1 : 0];
      triggers_pattern_mem <= mem_read_data[TIMER_WIDTH+SIGNALS_N+((TRIGGERS_N*3)+3)-1 : TIMER_WIDTH+SIGNALS_N];
    end
    else begin
      timer_pattern_mem    <= timer_pattern_mem;
      output_pattern_mem   <= output_pattern_mem;
      triggers_pattern_mem <= triggers_pattern_mem;
    end
  end
  
  assign mem_write_data = {triggers_pattern_conf, output_pattern_conf, timer_pattern_conf};

  // cross-domain signal syncers

  sync_reset #(
    .NEGATIVE(0),
    .STAGES(2) 
  ) sync_rst_gen (
    .clk(clk_gen),
    .arst_in(rst),  // i, async 
    .rst_out(rst_gen_2)   // o, sync to clk
  );

  sync_signal #(
      .WIDTH(2),
      .STAGES(2)
  ) sync_level_gen (
      .out_clk(clk_gen),
      .signal_in({conf_run_terminate, conf_run_start}),
      .signal_out({conf_run_terminate_Sgen, conf_run_start_Sgen})
  );

  sync_signal #(
      .WIDTH(RUN_COUNT_WIDTH),
      .STAGES(2)
  ) sync_level_conf_runs_n (
      .out_clk(clk_gen),
      .signal_in(conf_runs_n),
      .signal_out(conf_runs_n_Sgen)
  );

  sync_signal #(
      .WIDTH($clog2(PATTERN_MEM_DEPTH)),
      .STAGES(2)
  ) sync_level_mem_last_address (
      .out_clk(clk_gen),
      .signal_in(mem_last_address),
      .signal_out(mem_last_address_Sgen)
  );

  sync_signal #(
      .WIDTH(1),
      .STAGES(2)
  ) sync_level_cfg (
      .out_clk(clk_cfg),
      .signal_in(gen_running),
      .signal_out(stat_gen_running)
  );

  // submodules

  pattern_ram #(
    .RAM_WIDTH(TIMER_WIDTH + SIGNALS_N + (TRIGGERS_N*3)+3),
    .RAM_DEPTH(PATTERN_MEM_DEPTH)   // Specify RAM depth (number of entries)
  ) pattern_ram_inst (
    .addra(mem_write_address),
    .addrb(mem_read_address),
    .dina(mem_write_data),
    .clka(clk_cfg),
    .clkb(clk_gen),
    .wea(mem_write_strobe),
    .enb(1'b1),
    .rstb(rst_gen),
    .regceb(1'b1),
    .doutb(mem_read_data)
  );
  
  pattern_store #(
      .PATTERN_MEM_DEPTH(PATTERN_MEM_DEPTH)
    ) pattern_store_inst (
      .clk(clk_cfg),
      .rst(rst),
      .mem_write_strobe(mem_write_strobe),
      .mem_write_address(mem_write_address),
      .mem_last_address(mem_last_address),
      .timer_notzero(timer_conf_notzero),
      .conf_write_pattern(conf_write_pattern),
      .reset_generator(rst_gen_1)
    );
    
  pattern_load #(
      .PATTERN_MEM_DEPTH(PATTERN_MEM_DEPTH),
      .RUN_COUNT_WIDTH(RUN_COUNT_WIDTH),
      .TIMER_WIDTH(TIMER_WIDTH),
      .SIGNALS_N(SIGNALS_N),
      .TRIGGERS_N(TRIGGERS_N)
    ) pattern_load_inst (
      .clk(clk_gen),
      .rst(rst_gen),
    
      .mem_read_strobe(mem_read_strobe),
      .mem_read_address(mem_read_address),
      .mem_last_address(mem_last_address_Sgen),
      
      .timer_pattern_conf(timer_pattern_mem),
      .triggers_pattern_conf(triggers_pattern_mem),
      .triggers_in(triggers),
      .conf_runs_n(conf_runs_n_Sgen),
      .conf_run_terminate(conf_run_terminate_Sgen),
      .conf_run_start(conf_run_start_Sgen),
    
       .running(gen_running)
    );

endmodule